import "./projectDetails.css";
import Table from "react-bootstrap/Table";
import photo from "../images/zoomImg.png";
import 'react-inner-image-zoom/lib/InnerImageZoom/styles.css'
import InnerImageZoom from 'react-inner-image-zoom'


export function ProjectDetails(props) {
  return (
    <div className="projectDetails">
      <div className="pDetails-header"></div>
      <div className="details">
        <div className="d-client-info">
          <Table>
            <tbody>
              <tr>
                <td>Client Home</td>
                <td>{props.cname}</td>
              </tr>
              <tr>
                <td>Category</td>
                <td>Interiors</td>
              </tr>
              <tr>
                <td>Tags</td>
                <td>Interior, Home</td>
              </tr>
              <tr>
                <td>Date</td>
                <td>{props.date}</td>
              </tr>
              <tr>
                <td>Link</td>
                <td><a href="https://www.houzz.in/professionals/interior-designers-and-decorators/peaceful-homes-pfvwin-pf~2017756055?">Click here for Project Reviews</a></td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="d-project-info">
          <h2>Why Peaceful Homes by Smrita?</h2>
          <p>
          As an interior designer, each project I undertake is a canvas for creativity, precision, and client-centricity. Every space entrusted to my care is not merely a room to be decorated but a story waiting to be told, a reflection of the client's personality, taste, and aspirations. With meticulous attention to detail and a deep understanding of my client's desires, I weave together colors, textures, and furnishings to craft spaces that resonate with harmony and functionality. Each design is a collaborative journey, where the client's vision is the guiding star, and my expertise serves to bring it to life. From concept to completion, every step is carefully orchestrated to ensure that the end result exceeds expectations and embodies the client's best interests in every aspect.
            <br />
            <br />
            In the realm of interior design, my commitment remains unwavering: to transform spaces into personalized sanctuaries where beauty meets functionality, and where every detail is a testament to the client's unique style and satisfaction. Your dream space awaits, meticulously crafted with your best interests at heart.
          </p>
        </div>
      </div>
      <div className="zoomRoom">
      <InnerImageZoom src={photo} />
      </div>
    </div>
  );
}
