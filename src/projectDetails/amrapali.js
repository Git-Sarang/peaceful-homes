import { Link } from "react-router-dom";
import { ProjectDetails } from "./projectDetails";

export function Amrapali() {
    return (
        <ProjectDetails cname="Amrapali Saphire"
            date="2023"
        />
    );
}