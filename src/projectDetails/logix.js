import { Link } from "react-router-dom";
import { ProjectDetails } from "./projectDetails";

export function Logix() {
    return (
        <ProjectDetails cname="Logix Blossom County"
            date="2021"
        />
    );
}