import "./cookies.css";

export function Cookies() {
  return (
    <div className="cookies">
      <h1>Cookies Policy</h1>
      <p>
      we believe in the sweet blend of functionality and style, just like a perfectly baked cookie. Our cookie policy ensures that your online experience is as delightful and personalized as our interior designs. We use cookies to tailor content and ads, analyze site traffic, and enhance your browsing experience. Rest assured, your privacy is our priority, and we only use cookies with your consent. By continuing to indulge in our website, you agree to our cookie policy and the delicious journey it entails.
      </p>
      <p className="cookiesTitle">What are Cookies?</p>
      <p>
      Cookies are small pieces of data that websites store on your browser when you visit them. They serve various purposes, such as remembering your preferences, keeping you logged in, and tracking your activities on the site. Cookies can store information like login credentials, site preferences, shopping cart contents, and browsing history.{" "}
      </p>
      <p className="cookiesTitle">First-party cookies</p>
      <p>
      These cookies are created by the website you are currently visiting and can only be read by that website. They are commonly used to enhance the functionality of the website and provide a more personalized browsing experience. For instance, a first-party cookie might remember your preferred layout or display settings on a website.
      </p>
      <p className="cookiesTitle">Third-party cookies</p>
      <p>
      Third-party cookies are set by domains other than the one you are currently visiting. These cookies are often used for tracking purposes by advertisers and analytics services. For example, when you visit a website that contains embedded content, such as advertisements or social media widgets, third-party cookies may be set by the providers of that content to track your browsing activity across different websites. This information is then used to deliver targeted advertisements or analyze user behavior.
      </p>
      <p className="cookiesTitle">Essential cookies</p>
      <p>
        Quisque volutpat, justo id posuere semper, dolor arcu tincidunt est, ac
        convallis nulla nibh a dui. Vestibulum nunc enim, eleifend a urna sed,
        ultrices elementum neque. Duis cursus magna vel magna fringilla rutrum.
        Phasellus accumsan metus vel nunc sodales, eget vehicula lectus dictum.
        Sed ut blandit quam. Mauris lobortis lorem non malesuada maximus. Duis
        nec odio scelerisque, finibus ante vitae, venenatis nulla. Curabitur sit
        amet cursus nisl. Duis eu semper sem. Praesent sollicitudin orci at leo
        varius tempor. In sagittis mi non dolor mollis tincidunt. Donec
        fermentum erat eget purus iaculis consectetur. Morbi sed gravida arcu.
        Sed enim mi, mollis at ex non, consequat pharetra elit. Suspendisse nec
        ante consequat, pulvinar ligula in, imperdiet turpis. Morbi nec
        consectetur ipsum, nec elementum leo. Sed sed felis et massa fringilla
        tincidunt. Nulla rhoncus pretium leo, aliquet efficitur leo ultricies
        et. Nam fringilla nisl vel risus facilisis facilisis. Aliquam dictum,
        mauris nec fringilla congue, lectus est volutpat lectus, eu sagittis
        tellus enim et leo. Donec pretium magna vel ultrices ultrices. Sed
        imperdiet vestibulum tincidunt. Etiam sed velit tortor. Nam non lorem
        convallis, imperdiet lectus in, laoreet lorem. Cras ac odio metus.{" "}
      </p>
      <p className="cookiesTitle">Performance or analytical cookies</p>
      <p>
      Performance cookies, also known as analytics cookies, are a type of cookie that collect information about how visitors use a website. Unlike other types of cookies, such as those used for advertising or social media integration, performance cookies are primarily focused on improving website performance and user experience rather than targeting specific users for marketing purposes.
      </p>
      <p className="cookiesTitle">Social media cookies</p>
      <p>
      Social media cookies are a type of third-party cookie that are set by social media platforms when you interact with social media content on a website. These cookies enable social media platforms to track your browsing activity across different websites and gather information about your interests and behavior for targeted advertising and analytics purposes.

      </p>
    </div>
  );
}
