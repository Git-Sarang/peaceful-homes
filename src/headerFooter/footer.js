import {
  FaInstagram,
  FaFacebookF,
  FaTwitter,
  FaLinkedin,
} from "react-icons/fa";
import { Link } from "react-router-dom";
import logo from "../images/logo.png";
import "./footer.css";

export function Footer() {
  return (
    <div className="footer">
      <div className="f-info">
        <section className="f-social-medias">
          <div className="f-logo-text">
            <Link style={{ display: "flex" }} to="/">
              <div className="f-logo">
                <img src={logo} alt="logo"></img>
              </div>
              <div className="f-text">
                <p>Peaceful Homes</p>
              </div>
            </Link>
          </div>
          <div className="f-about-text">
            <p>
            "A home you Never want to leave"; Connecting people with style.

            </p>
          </div>
          <div className="f-smedia">
            <ul>
              <li>
                <a href="https://www.instagram.com/peacefulhome.design/">
                  <FaInstagram />
                </a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/smrita-gupta-9a510832/">
                  <FaLinkedin />
                </a>
              </li>
            </ul>
          </div>
        </section>
        <section className="f-pages">
          <p>Pages</p>
          <ul>
            <li>
              <Link to={`/aboutus`}>About Us</Link>
            </li>
            <li>
              <Link to={`/projects`}>Our Projects</Link>
            </li>
            <li>
              <Link to={`/team`}>Our Team</Link>
            </li>
            <li>
              <Link to={`/contact`}>Contact Us</Link>
            </li>
            <li>
              <Link to={`/services`}>Services</Link>
            </li>
          </ul>
        </section>
        <section className="f-services">
          <p>Services</p>
          <ul>
            <li>
              <Link to={`/`}>Kitchen</Link>
            </li>
            <li>
              <Link to={`/`}>Living Room</Link>
            </li>
            <li>
              <Link to={`/`}>Bathroom</Link>
            </li>
            <li>
              <Link to={`/`}>Dinning Hall</Link>
            </li>
            <li>
              <Link to={`/`}>Bedroom</Link>
            </li>
          </ul>
        </section>
        <section className="f-contact">
          <p>Contact</p>
          <p>Noida, Uttar Pradesh, 201305</p>
          <p>
            <a href="mailto: peacefuldesign.home@gmail.com">peacefuldesign.home@gmail.com</a>
          </p>
          <p>+(91) 88265 77663</p>
        </section>
      </div>
      <div className="terms">
        <ul>

          <ol>
            <Link to={`/cookies-policy`}>&bull; Cookies Policy</Link>
          </ol>
          <ol>
            <Link to={`/faq`}>&bull; FAQ</Link>
          </ol>
        </ul>
      </div>
      <div className="f-copyright">
        <p>© Smrita Gupta Interiors and furnishing | Website Designed by Sarang Rawat</p>
      </div>
    </div>
  );
}
