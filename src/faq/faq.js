import "./faq.css";
import pQuestion from "../images/projectQuestion.jpg";
import eQuestion from "../images/everyQuestion.jpg";
import { IoIosArrowForward } from "react-icons/io";
import { useState } from "react";

export function Faq() {
    const [showQ1, setShowQ1] = useState(false);
    const [showQ2, setShowQ2] = useState(false);
    const [showQ3, setShowQ3] = useState(false);
    const [showQ4, setShowQ4] = useState(false);
    const [showQ5, setShowQ5] = useState(false);
    const [showQ6, setShowQ6] = useState(false);
    const [showQ7, setShowQ7] = useState(false);
    const [showQ8, setShowQ8] = useState(false);
    const [showQ9, setShowQ9] = useState(false);
    const [showQ10, setShowQ10] = useState(false);

  return (
    <div className="faq">
      <div className="faq-header">
        <h1>
          Faq's<p>Home / Faq</p>
        </h1>
      </div>
      <h1 className="faq-title">Every Question Answered</h1>
      <div className="e-question">
        <div className="ep-questions">
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ1(!showQ1);
              }}
            >
              What is the typical timeline for completing an interior design project?{" "}
              <IoIosArrowForward />
            </p>

            {showQ1 ? (
              <p className="q-content">
                The timeline for completing an interior design project can vary depending on the scope and complexity of the project. A small-scale project, such as redecorating a single room, may take a few weeks to a couple of months to complete. Larger projects, such as remodeling an entire home or commercial space, can take several months to a year or more. Factors that can influence the timeline include the availability of materials, contractors, and the extent of customization required.
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ2(!showQ2);
              }}
            >
              How much does an interior design project cost?{" "}
              <IoIosArrowForward />
            </p>

            {showQ2 ? (
              <p className="q-content">
                The cost of an interior design project varies widely depending on factors such as the size of the space, the level of customization, the quality of materials and furnishings, and the location of the project. 
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ3(!showQ3);
              }}
            >
              How involved will I be in the decision-making process?{" "}
              <IoIosArrowForward />
            </p>

            {showQ3 ? (
              <p className="q-content">
                The level of client involvement in the decision-making process can vary depending on the preferences of the client. Some clients prefer to be very hands-on and involved in every decision, while others prefer to give the designer more creative freedom. A good designer will work closely with the client to understand their vision, preferences, and budget, and will keep them informed and involved throughout the design process.
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ4(!showQ4);
              }}
            >
              Do I need to have a specific style in mind for my interior design project?{" "}
              <IoIosArrowForward />
            </p>

            {showQ4 ? (
              <p className="q-content">
                While having a specific style preference can be helpful in guiding the design process, it's not necessarily required. A skilled interior designer can work with clients to explore different styles, identify elements they like and dislike, and develop a cohesive design concept that reflects their tastes and lifestyle. Whether you prefer modern, traditional, eclectic, or something in between, a good designer can tailor the design to suit your preferences.
              </p>
            ) : null}
          </div>
          
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ5(!showQ5);
              }}
            >
             Can you work with my existing furniture and decor, or do I need to purchase all new items?{" "}
              <IoIosArrowForward />
            </p>

            {showQ5 ? (
              <p className="q-content">
                We are skilled at incorporating existing furniture and decor into their designs, especially if those items hold sentimental value or have special meaning to the client. However, in some cases, it may be necessary to purchase new furnishings or decor to achieve the desired look and feel for the space. A good designer will work with the client to determine which items can be reused and which should be replaced or supplemented with new pieces.
              </p>
            ) : null}
          </div>
        </div>
        <div className="ep-q-img">
          <img src={eQuestion} alt="everyquestion"></img>
        </div>
      </div>



      <h1 className="faq-title">Project related questions</h1>
      <div className="p-question">
        <div className="ep-q-img">
          <img src={pQuestion} alt="projectquestion"></img>
        </div>
        <div className="ep-questions">
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ6(!showQ6);
              }}
            >
              What is the first step in starting an interior design project?{" "}
              <IoIosArrowForward />
            </p>

            {showQ6 ? (
              <p className="q-content">
                The first step typically involves an initial consultation between the designer and the client to discuss project goals, budget, timeline, and design preferences.
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ7(!showQ7);
              }}
            >
              
            How do I choose the right interior designer for my project?{" "}
              <IoIosArrowForward />
            </p>

            {showQ7 ? (
              <p className="q-content">
                Researching designers' portfolios, reading reviews, and scheduling consultations are good ways to find the right fit. It's crucial to select a designer whose style, approach, and personality align with your vision and needs.
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ8(!showQ8);
              }}
            >
              Why should I hire an interior designer instead of doing it myself?{" "}
              <IoIosArrowForward />
            </p>

            {showQ8 ? (
              <p className="q-content">
                Interior designers bring expertise, creativity, and industry connections to the table, ensuring that your space is not only aesthetically pleasing but also functional and tailored to your lifestyle. They can save you time, money, and stress by managing every aspect of the design process with efficiency and professionalism.
              </p>
            ) : null}
          </div>
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ9(!showQ9);
              }}
            >
              Can an interior designer help with sustainable design practices?{" "}
              <IoIosArrowForward />
            </p>

            {showQ9 ? (
              <p className="q-content">
                Absolutely! Many interior designers are well-versed in sustainable design principles and eco-friendly materials, helping you create a healthier and more environmentally conscious living or working environment.
              </p>
            ) : null}
          </div>
          
          <div className="ep-q">
            <p
              className="q-title"
              onClick={() => {
                setShowQ10(!showQ10);
              }}
            >
              What if unexpected issues arise during the project?{" "}
              <IoIosArrowForward />
            </p>

            {showQ10 ? (
              <p className="q-content">
                We anticipate potential challenges and are equipped to handle them efficiently. Open communication and flexibility are key to addressing unexpected issues while staying on track with the project timeline and budget.
              </p>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}
