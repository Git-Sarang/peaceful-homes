import "./team.css";
import m0 from "../images/team/member1.png";
import m1 from "../images/team/member2.jpg";
import data from "./team_members.json";
import { FaFacebookF, FaTwitter, FaLinkedin } from "react-icons/fa";
import { Link } from "react-router-dom";

export function Team() {
  return (
    <div className="team">
      <div className="team-header">
        <h1>
          Our Professional<p>Home / Team</p>
        </h1>
      </div>
      <div className="team-members">
        {data.members
          .filter((m, ind) => ind < 2)
          .map((mbr, i) => {
            const image = i % 2 === 0 ? m0 : m1;
            return (
              <Link to={mbr.lknd}>
              <div className="member" key={i}>
                <div className="member-photo">
                  <img src={image} alt="member"></img>
                </div>
                <div className="member-info">
                  <p className="fullname">{mbr.fullname}</p>
                  <p className="m-detail">
                    {mbr.job},{mbr.country}
                  </p>
                  <ul>
                    <li>
                      <a href={mbr.lknd}>
                        <FaLinkedin />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              </Link>
            );
          })}
      </div>
    </div>
  );
}
