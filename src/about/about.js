import "./about.css";
import whatWe from "../images/whatwedo.jpg";
import result from "../images/result.jpg";
import { BsArrowRight } from "react-icons/bs";

export function About() {
  return (
    <div className="about">
      <div className="about-header">
        <h1>
          About Us<p>Home / About us</p>
        </h1>
      </div>
      <div className="quotes">
        <div className="pattern first">
          <p></p>
        </div>
        <div className="content">
          <h1>“</h1>
          <p className="comment">
            I like an interior that defies labeling. I don't really want someone
            to walk into a room and know that I did it
          </p>
          <p className="author">- SMRITA GUPTA</p>
        </div>

        <div className="pattern second"></div>
      </div>
      <div className="about-concept">
        <div className="con firstPart">
          <div className="c-text">
            <h1>What We Do</h1>
            <p>
            From cozy living rooms to functional kitchens and serene bedrooms, we blend aesthetics with functionality to create spaces that are both beautiful and practical. Whether you prefer modern minimalism, classic elegance, or eclectic charm, our team is dedicated to bringing your vision to life with attention to detail and a commitment to quality.
            </p>
            <button>
              Our Concept
              <BsArrowRight style={{ color: "#CDA274", marginLeft: "3%" }} />
            </button>
          </div>
          <div className="concept-img">
            <img src={whatWe} alt="concept"></img>
          </div>
        </div>

        <div className="con secondPart">
          <div className="concept-img">
            <img src={result} alt="concept"></img>
          </div>
          <div className="c-text">
            <h1>What We Do</h1>
            <p>
            We're passionate about crafting interior spaces that tell a story, merging functionality with aesthetics to create homes that resonate with warmth and personality.
            </p>
            <button>
              Our Concept
              <BsArrowRight style={{ color: "#CDA274", marginLeft: "3%" }} />
            </button>
          </div>
        </div>
      </div>

    </div>
  );
}
